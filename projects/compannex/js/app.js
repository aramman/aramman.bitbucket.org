var config = {};

// Configure responsive bootstrap toolkit
config.ResponsiveBootstrapToolkitVisibilityDivs = {
    'xs': $('<div class="device-xs 				  hidden-sm-up"></div>'),
    'sm': $('<div class="device-sm hidden-xs-down hidden-md-up"></div>'),
    'md': $('<div class="device-md hidden-sm-down hidden-lg-up"></div>'),
    'lg': $('<div class="device-lg hidden-md-down hidden-xl-up"></div>'),
    'xl': $('<div class="device-xl hidden-lg-down			  "></div>'),
};

ResponsiveBootstrapToolkit.use('Custom', config.ResponsiveBootstrapToolkitVisibilityDivs);

//validation configuration
config.validations = {
	errorClass:'has-error',
	validClass:'success',
	errorElement:"span",

	// add error class
	highlight: function(element, errorClass, validClass) {
		$(element).parents("div.form-group")
		.addClass(errorClass)
		.removeClass(validClass); 
	}, 

	// add error class
	unhighlight: function(element, errorClass, validClass) {
		$(element).parents(".has-error")
		.removeClass(errorClass)
		.addClass(validClass); 
	}
};
//LoginForm validation
$(function() {

	// set login form
	var $loginForm = $('#login-form');

	if (!$loginForm.length) {
        return false;
    }

    // set login validations
    var loginValidationSettings = {
	    rules: {
	        identifier: {
	            required: true,
	        },
	        password: {
	            required: true,
	        }     
	    },
	    messages: {
	        identifier: {
	            required: "Please enter username",
	        },
	        password:  {
	            required: "Please enter password",
	        }
	    }
	}

	// merger default and login validations
	$.extend(loginValidationSettings, config.validations);

	// validate fields
    var $loginValidator = $loginForm.validate(loginValidationSettings);    

    // set loader
    var $loginLoader = $('#loading');

    // initialize the ajax form plugin
    $loginForm.ajaxForm({  	
        // any other options,
        beforeSubmit: function () {
        	$loginLoader.addClass('active');

            return $loginForm.valid(); // TRUE when form is valid, FALSE will cancel submit
        },
        error: function (data) {
        	$loginLoader.removeClass('active');

            $loginValidator.showErrors({
				"identifier": data.responseJSON.message,
				"password": ""
			})
        },
        success: function (data) {

        	// set cookie options
        	var $cookieOptions = {
        		path: '/',
        		expires: 1
        	}

        	// remember user
        	if ($('.checkbox').is('checked')) {
        		$cookieOptions.expires = 365;
        	}

        	// set token in cookie
        	$.cookie('token', data.token, $cookieOptions);

           	$(location).attr('href', '/');	
        }
    });

});
//RegisterForm validation
$(function() {

    //setSameHeights();

	var $authWrapper = $('.auth-wrapper');
	var $signupCompanyForm = $('#register-company-form');
	var $signupConsultantForm = $('#register-consultant-form');

	/*if (
		!$signupCompanyForm.length ||
		!$signupConsultantForm.length
	) {
        return false;
    }*/


    $('.btn-start').on('click', function(e){
    	e.preventDefault();

    	var $formContainer = $('.form-container');

        // openig form container
        $formContainer.addClass('form-opened');

    	// remove .active class from auth-box
    	$formContainer.find('.auth-box')
    	.removeClass('active');

    	// get selected auth-box id
    	var $selectedBoxId = $(this).attr('href');

    	// add .active class to selected box
    	$($selectedBoxId).addClass('active');

    	// set .move-left class to auth-wrapper
    	$authWrapper.addClass('open-right-box');

        //setSameHeights();
    });

});
/* Demo Scripts for Bootstrap Carousel and Animate.css article
* on SitePoint by Maria Antonietta Perna
*/
$(function() {

	//Function to animate slider captions 
	function doAnimations(elems) {
		//Cache the animationend event in a variable
		var animationEndEvent = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
		
		elems.each(function () {
			var $this = $(this);
			var $animationType = $this.data('animation');

			$this.addClass($animationType)
			.one(animationEndEvent, function() {
				$this.removeClass($animationType);
			});
		});
	}
	
	//Variables on page load 
	var $myCarousel = $('#slider');
	var $firstAnimatingElems = $myCarousel.find('.item:first').find("[data-animation ^= 'animated']");
		
	//Initialize carousel 
	$myCarousel.carousel();
	
	//Animate captions in first slide on page load 
	doAnimations($firstAnimatingElems);
	
	//Pause carousel  
	//$myCarousel.carousel('pause');
	
	
	//Other slides to be animated on carousel slide event 
	$myCarousel.on('slide.bs.carousel', function (e) {
		var $animatingElems = $(e.relatedTarget).find("[data-animation ^= 'animated']");
		doAnimations($animatingElems);
	});  
	
});
$(function(){
	var $wrapper = $('.main-wrapper');

	if (!$wrapper.length) {
		return false;
	}

	/****************************************
	*
	*			Header Block
	*
	****************************************/

	$(window).scroll(function(){
		var $header = $('.header'),
		scroll = $(window).scrollTop();

		if (scroll >= 40) {
			$header.addClass('fixed-header');
		}
		else {
			$header.removeClass('fixed-header');
		}

	});

	/****************************************
	*
	*			Sidebar Block
	*
	****************************************/

	$('.btn-toggle-menu').on('click', function(){
		$wrapper.addClass('sidebar-opened');
	})

	$(document).on('click', function(event){
		$clickedElem = $(event.target);

		if ($clickedElem.hasClass('sidebar-wrapper')) {
			$wrapper.removeClass('sidebar-opened');
		}
	});

	/****************************************
	*
	*			Search Block
	*
	****************************************/
	var $authSearch = $('.auth-search');

	$('.search-icon').on('click', function() {
		$authSearch.addClass('open');
		animate({
			name: 'fadeInDown',
			selector: $authSearch
		});	
	});

	$('.search-close').on('click', function() {
		animate({
			name: 'fadeOutUp',
			selector: $authSearch
		}, function() {
			$authSearch.removeClass('open');
		});	
	});
})
// set loader
$(window).load(function() {
	// Animate loader off screen
	$("#main-loader").fadeOut("slow", function(){
		$(this).removeClass('active')
		.remove();
	});
});

$(function(){

	$('.nav-menu > .menu a, .sidebar-menu a').on("click", function(event) {
		event.preventDefault();

		// remove active class
		$('.nav-menu > .menu a, .sidebar-menu a').removeClass('active');

		// set active class
		$(this).addClass('active');


		// set selector
		var $selector = $(this).attr('href');

		// check if header fixed or not
		var $headerFixed = $('.header').hasClass('fixed-header');

		if ($headerFixed) {
			var scrollTo = $($selector).offset().top - 100;
		}
		else {
			var scrollTo = $($selector).offset().top - 200;
		}

		
		$("html, body").animate({
			scrollTop: scrollTo
		}, 1000);
	});
});

function setSameHeights($container) {

	$container = $container || $('.sameheight-container');

	var viewport = ResponsiveBootstrapToolkit.current();

	$container.each(function() {

		var $items = $(this).find(".sameheight-item");

		// Get max height of items in container
		var maxHeight = 0;

		$items.each(function() {
			$(this).css({height: 'auto'});
			maxHeight = Math.max(maxHeight, $(this).innerHeight());
		});


		// Set heights of items
		$items.each(function() {
			// Ignored viewports for item
			var excludedStr = $(this).data('exclude') || '';
			var excluded = excludedStr.split(',');

			// Set height of element if it's not excluded on 
			if (excluded.indexOf(viewport) === -1) {
				$(this).innerHeight(maxHeight);
			}
		});
	});as
}

/*******************************
*        Animation Settings
*******************************/

function animate(options, callback) {
	var animationName = "animated " + options.name;
	var animationEnd = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
	$(options.selector)
	.addClass(animationName)
	.one(animationEnd, 
		function(){
			$(this).removeClass(animationName);

			if (callback) {
				callback();
			}
		}
	);
}