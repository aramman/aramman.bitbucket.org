
scrollOnClickDirective.$inject = ["$log"];var softnetApp = {};

softnetApp.main = angular.module("softnetApp", [

	// Library modules
	'ui.router',
	'ngCookies', 
	'angular-storage', 
	'angular-jwt',
	'spring-data-rest',

	// Vendor 
	'angularModalService',
	'angular-flexslider',
	'frapontillo.bootstrap-switch',
	
	// Application cached tempaltes
	'softnetApp.templates',
	
	// Application modules
	'softnetApp.common',
	'softnetApp.app',
	'softnetApp.auth',
	'softnetApp.popupPages'
]);

// Common components module
softnetApp.common = angular.module("softnetApp.common", []);

// App module
softnetApp.app = angular.module("softnetApp.app", []);

// Auth module
softnetApp.auth = angular.module("softnetApp.auth", []);

// Popup page module
softnetApp.popupPages = angular.module("softnetApp.popupPages", []);
softnetApp.main

.constant('Config', {
	api: {
		url: window.location.protocol + "//" + window.location.host + "/api",
	},
	app: {
		url: window.location.protocol + "//" + window.location.host,
		defaultState: 'app.home',
	}
})
softnetApp.main

.config(["$httpProvider", "jwtInterceptorProvider", function($httpProvider, jwtInterceptorProvider) {


	// set default content types application/x-www-form-urlencoded
	/*$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';


	$httpProvider.defaults.transformRequest = [function(data){
		if (
			angular.isObject(data) && 
			String(data) !== '[object File]'
		) {
			return encodeURIObject(data);
		}
		else {
			return data;
		}
	}];*/

	// JWT interceptor will take care of sending the JWT in every request.
	jwtInterceptorProvider.tokenGetter = function($cookies, $log, authService) {
		
		return authService.getToken();
	};

	$httpProvider.interceptors.push('jwtInterceptor');

}]);


//converting object to x-www-form-urlencoded string.
var encodeURIObject = function (obj) {

	var query = '';
	var name, value, 
	fullSubName, subValue, innerObj, i;

	for(name in obj) {
		value = obj[name];

		if(value instanceof Array) {
			for(i=0; i<value.length; ++i) {
				subValue = value[i];
				fullSubName = name + '[' + i + ']';
				innerObj = {};
				innerObj[fullSubName] = subValue;
				query += encodeURIObject(innerObj) + '&';
			}
		}

		else if(value instanceof Object) {
			for(subName in value){
				subValue = value[subName];
				fullSubName = name + '[' + subName + ']';
				innerObj = {};
				innerObj[fullSubName] = subValue;
				query += encodeURIObject(innerObj) + '&';
			}
		}

		else if(value !== undefined && value !== null){
			query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
		}
	}

	if (query.length) {
		return query.substr(0, query.length - 1)
	}

	return query;
}
softnetApp.main

.controller("MainCtrl", ["$log", function ($log) {

	var vm = this; 

}]);

softnetApp.main

.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {

	$urlRouterProvider.otherwise(function($injector) {
		var $state = $injector.get("$state");
		$state.go("app.home");
	});	
}])

.run(["$rootScope", "$log", "$state", "authService", "Config", function ($rootScope, $log, $state, authService, Config) {
	$rootScope.uiState = {};

	$rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){


		// If the route requires login, and user is not logged in
		// Redirect to login
		if(
			angular.isDefined(toState.data) &&
			toState.data.requiresLogin &&
			!authService.isAuthenticated()
		) {
			event.preventDefault();

			$state.go("auth.login", {
				redirectState: toState.name
			});
		}


		// If  the route is disabled after login, and user is logged in
		// Redirect to app default state
		if (
			toState.name != Config.app.defaultState &&
			toState.name != fromState.name &&
			angular.isDefined(toState.data) &&
			toState.data.disabledAfterLogin &&
			authService.isAuthenticated()
		) {
			event.preventDefault();

			$state.go(Config.app.defaultState);
		}
  	});

}]);
softnetApp.app

.controller("AppCtrl", ["$log", "authService", "user", function ($log, authService, user) {

	var vm = this; 

	// check is user logined
	vm.isAuth = authService.isAuthenticated();
	
	// set user data
	vm.user = user;

	/*if (
		vm.isAuth && 
		!vm.user.data.notifications
	) {
		vm.user.data.notifications = {};
		vm.user.data.notifications.sms = false;
		vm.user.data.notifications.email = false;
	}*/
}]);

softnetApp.app

.config(["$stateProvider", function($stateProvider) {

	$stateProvider

	// Dashboard state
	.state('app', {
		abstract : true,
		templateProvider: ["$templateCache", function($templateCache) {  
			return $templateCache.get('templates/app/app.html'); 
		}],
		controller: "AppCtrl as app",
		data: {},
		resolve : {
			user: ["authService", "usersService", function( authService, usersService){
				return {};//(authService.isAuthenticated())? usersService.getProfile(): {}; 
			}]
		}
	})

	// App state
	.state('app.home', {
		url: "/",
		templateProvider: ["$templateCache", function($templateCache) {  
			return $templateCache.get('templates/app/home/home.html'); 
		}],
		controller: "HomeCtrl as home",
		data: {
			
		}
	})

	.state('app.careers', {
		url: "/careers",
		templateProvider: ["$templateCache", function($templateCache) {  
			return $templateCache.get('templates/app/careers/careers.html'); 
		}],
		controller: "CareersCtrl as careers",
		data: {
			requiresLogin: true,
		}
	});
}]);
softnetApp.auth

.controller("AuthCtrl", ["$log", "ModalService", function ($log, ModalService) {

	var vm = this; 

	/********************************************
	*				SHOW MODAL
	*********************************************/
	//open a sign up modal
	vm.openSignupModal = function () {

		// Just provide a template url, a controller and call 'showModal'.
		ModalService.showModal({
			templateUrl: 'templates/auth/signup-modal/signup-modal.html',
			controller: 'SignupModalCtrl',
			controllerAs: 'signupModal'
		})
		.then(function(modal) {
			
			modal.close.then(function(result) {
				$log.log(result);
				$('.modal-backdrop').remove();
			});
		});
	}


	//open an activation modal
	vm.openActivationModal = function () {
		
		// Just provide a template url, a controller and call 'showModal'.
		ModalService.showModal({
			templateUrl: 'templates/auth/activation-modal/activation-modal.html',
			controller: 'ActivationModalCtrl',
			controllerAs: 'activationModal'
		})
		.then(function(modal) {
			
			modal.close.then(function(result) {
				$log.log(result);
			});
		});
	}


	//open a forfot pasword modal
	vm.openForgotPassModal = function () {

		// Just provide a template url, a controller and call 'showModal'.
		ModalService.showModal({
			templateUrl: 'templates/auth/forgot-pasword-modal/forgot-pasword-modal.html',
			controller: 'ForgotPassModalCtrl',
			controllerAs: 'forgotPassModal'
		})
		.then(function(modal) {
			
			modal.close.then(function(result) {
				$log.log(result);
			});
		});
	}

}]);

softnetApp.auth

.config(["$stateProvider", function($stateProvider) {

	$stateProvider

	// auth state
	.state('auth', {
		abstract : true,
		templateProvider: ["$templateCache", function($templateCache) {  
			return $templateCache.get('templates/auth/auth.html'); 
		}],
		controller: "AuthCtrl as auth"
	})

	// login state
	.state('auth.login', {
		url: "/login",
		templateProvider: ["$templateCache", function($templateCache) {  
			return $templateCache.get('templates/auth/login/login.html'); 
		}],
		controller: "LoginCtrl as login",
		data: {
			disabledAfterLogin: true
		}
	})

	.state('auth.logout', {
		url: "/logout",
		controller: "LogoutCtrl as logout",
		data: {}
	});

}]);
/*
*	Auth service
*/

softnetApp.auth

.factory("authService", ["$http", "$log", "localStorage", "Config", "SpringDataRestAdapter", function($http, $log, localStorage, Config, SpringDataRestAdapter) {

	var auth = {
		token: null
	};

	return {
		login: function(data) {

			var httpPromise = $http.post(Config.app.url + '/login', data, {
				headers: { 
					'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
				},
				transformRequest: encodeURIObject
			});

			return SpringDataRestAdapter.process(httpPromise);
		},

		register: function(data){
			var httpPromise = $http.post(Config.app.url + '/register', data);

			return SpringDataRestAdapter.process(httpPromise);
		},

		logout: function() {
			var httpPromise = $http.post(Config.app.url + '/logout');

			return SpringDataRestAdapter.process(httpPromise);
		},

		/*resendActivationCode: function(data){
			var httpPromise = $http.post(Config.app.url + '/resendactivationcode', data);

			return SpringDataRestAdapter.process(httpPromise);
		},

		// Reset password
		resetPass: function(data) {
			var httpPromise = $http.post(Config.app.url + '/reset', data);
			
			return SpringDataRestAdapter.process(httpPromise);
		},*/

		// Send email with reset link 
		/*recover: function(identifier) {
			return $http.post(Config.api.url + "/auth/recover", {
				identifier: identifier
			});
		},*/

		// Checks if user is autenticated
		isAuthenticated: function() {
			/*if(angular.isDefined(auth.token) && auth.token) {
				return true;
			}
			else if(localStorage.get('token')) {
				return true;
			}
			else {
				return false;
			}*/
			return true;
		},

		// Set authentication token
		setToken: function(token) {

			// Set token in local storage
			localStorage.set('token', token);

			auth.token = token;
		},

		// Remove user token 
		removeToken: function() {
			// Remove token from local storage
			localStorage.remove('token');

			auth.token = null;
		},

		// Complete remove
		remove: function() {
			this.removeToken();
		},

		getToken: function() {
			return auth.token || localStorage.get('token');
		},

		get: function() {
			return auth;
		}
	}
}]);
softnetApp.app

.controller("CareersCtrl", ["$log", "user", "careesService", function ($log, user, careesService) {

	var vm = this; 


	vm.options = {
		//title: 'Table of people',
		paging: true, 
		pageSize: 4, 
		sorting: true,
		defaultSorting: 'Name ASC',
		actions: {
			listAction: function(postData, jtParams) {
				return careesService.getCarees();
			},
			/*createAction: '/GettingStarted/CreatePerson',
			updateAction: '/GettingStarted/UpdatePerson',
			deleteAction: '/GettingStarted/DeletePerson'*/
		},
		fields: {
			PersonId: {
				key: true,
				list: false
			},
			Name: {
				title: 'Author Name',
				width: '40%'
			},
			Age: {
				title: 'Age',
				width: '20%'
			},
			RecordDate: {
				title: 'Record date',
				width: '30%',
				type: 'date',
				create: false,
				edit: false
			}
		}
	}

	$log.log('Careers Controller');
}]);

softnetApp.app

.directive('careers', ["$compile", "$timeout", function($compile, $timeout){
    return {
    	scope: {
    		options: '='
    	},
        link: function($scope, $element, $attrs) {

            $element.jtable($scope.options);

	        $element.jtable('load');

        }
    }
}]);


/*
*	Carees service
*/

softnetApp.app

.factory("careesService", ["$http", "$log", "Config", "SpringDataRestAdapter", function($http, $log, Config, SpringDataRestAdapter) {
	return {
		getCarees: function(){
			var httpPromise = $http.get(Config.app.url + '/projects/isofnet/assets/data/careers.json');

			return $.Deferred(function ($dfd) {

				SpringDataRestAdapter.process(httpPromise)

				.then(
					// success
					function(data){
						$dfd.resolve(data);
					}, 
					// error
					function(error){
						$dfd.reject();
						$log.log(error);
					}
				);
			});
		}
	}
}]);
softnetApp.app

.controller("HomeCtrl", ["$log", function ($log) {

	var vm = this; 
	
	$log.log("Home Controller");
}]);

softnetApp.app

.controller("EditProfileCtrl", ["$scope", "$log", "usersService", "helperService", function ($scope, $log, usersService, helperService) {

	var vm = this; 

	// set validation variables
	vm.loading = false;
	vm.error = {};

	$scope.forms = {
		editProfile: {},
		editPassword: {},
		editNotes: {}
	};

	// update profile
	vm.updateProfile = function(data) {

		if ($scope.forms.editProfile.$invalid) {
			return false;
		}

		vm.loading = true;

		usersService.updateUser(data)

		.then(function(response){

			vm.loading = false;

			$.fancybox.close();
		})

		.catch(function(error){
			// set error
			vm.error.show = true;
			vm.error.message = error.data.errorMessage;

			vm.loading = false;
		});

	}

	vm.updatePassword = function(){
		if ($scope.forms.editPassword.$invalid) {
			return false;
		}

		vm.loading = true;

		usersService.updateUserPassword({
			oldPwd: vm.password,
			newPwd: vm.newpassword
		})

		.then(function(response){

			vm.loading = false;

			$.fancybox.close();	
		})

		.catch(function(error){
			
			// set error
			vm.error.show = true;
			vm.error.message = error.data.errorMessage;

			vm.loading = false;
		});
	}

	vm.updateNotificatons = function(data){

		if ($scope.forms.editNotes.$invalid) {
			return false;
		}

		vm.loading = true;

		usersService.updateUser(data)

		.then(function(response){

			vm.loading = false;

			$.fancybox.close();
		})

		.catch(function(error){
			// set error
			vm.error.show = true;
			vm.error.message = error.data.errorMessage;

			vm.loading = false;
		});
	}

	vm.cleanError =function() {
		vm.error.show = false;
		vm.error.message = '';
	}
}]);

softnetApp.auth

.controller("ActivationModalCtrl", ["$scope", "$log", "close", "authService", function ($scope, $log, close, authService) {

	var vm = this; 

	vm.loading = false;
	vm.error = '';
	vm.activateEmail = '';
	

	vm.requestActiveCode = function(){

		if ($scope.activationForm.$invalid) {
			return false;
		}

		vm.loading = true;

		authService.resendActivationCode({
			email: vm.activateEmail
		})

		.then(function(result){

			vm.loading = false;

		})

		.catch(function() {
			vm.error = "EMail is NOT Registered";
			vm.loading = false;
		});

	}

	//Close Modal
	vm.closeModal = function(result) {
	    close(result, 500); // close, but give 200ms for bootstrap to animate
	};

	$log.log("ActivationModalCtrl");

}]);

softnetApp.auth

.controller("ForgotPassModalCtrl", ["$scope", "$log", "close", "authService", function ($scope, $log, close, authService) {

	var vm = this; 


	vm.loading = false;
	vm.error = '';
	vm.forgotPassEmail = '';
	
	vm.requestForgotPass = function(){

		if ($scope.forgotPassForm.$invalid) {
			return false;
		}

		vm.loading = true;

		authService.resetPass({
			email: vm.forgotPassEmail
		})

		.then(function(result){

			vm.loading = false;

		})

		.catch(function() {
			vm.error = "EMail is NOT Registered";
			vm.loading = false;
		});

	}

	//Close Modal
	vm.closeModal = function(result) {
	    close(result, 500); // close, but give 200ms for bootstrap to animate
	};

	$log.log("ForgotPassModalCtrl");

}]);

softnetApp.auth

.controller("LoginCtrl", ["$scope", "$log", "$state", "$window", "authService", "Config", function ($scope, $log, $state, $window, authService, Config) {

	var vm = this;

	vm.loading = false;
	vm.error = {
		show: false,
		message: ''
	};

	vm.data = {};

	vm.login = function(){

		if ($scope.loginForm.$invalid) {
			return false;
		}
		
		vm.cleanError();
		vm.loading = true;

		authService.login(vm.data)

		.then(function(result){

			vm.loading = false;

			// set token and redirect user to default state
			authService.setToken(result.token);
			
			$state.go(Config.app.defaultState);
		})

		.catch(function(error) {
			// log errors
			$log.log(error.data);

			// set error and show it
			vm.error.show = true
			vm.error.message = 'Invalid username and/or password';
			
			vm.loading = false;
		});

	}

	vm.cleanError = function() {
		vm.error.show = false;
		vm.error.message = '';
	}

}]);

softnetApp.auth

.controller("LogoutCtrl", ["$log", "$state", "authService", function ($log, $state, authService) {

	var vm = this; 

	authService.logout()

	.then(function(){

		// remove token
		authService.remove();

		// redirect state to login page
		$state.go('auth.login');
	})

	.catch(function(error){
		$log.log(error);
	});
}]);

softnetApp.auth

.controller("SignupModalCtrl",  ["$scope", "$log", "close", "authService", "helperService", function ($scope, $log, close, authService, helperService) {

	var vm = this; 

	// set validation variables
	vm.loading = false;
	vm.error = '';

	// set data variables
	vm.userData = {};

	// set company default data
	vm.userData.company = {};

	// get all countries
	helperService.getCountries()

	.then(function(countries){
		vm.countries = countries;
	});

	/*****************************************
	*	
	*			  Signup Policy
	*		
	*****************************************/

	// set policy variables
	vm.policyRead = false;
	vm.policyAggree = false;

	vm.readPloicy = function(){
		vm.policyRead = true;
	}

	vm.agreePloicy = function(){
		vm.policyAggree = true;
	}

	/*****************************************
	*	
	*			  Signup Form
	*		
	*****************************************/
	$scope.form = {
		signup: {}
	};

	// regirster user
	vm.register = function() {

		if (
			$scope.form.signup.$invalid ||
			!vm.policyAggree ||
			!vm.policyRead
		) {
			return false;
		}

		vm.loading = true;

		vm.userData.tncAccepted = vm.policyAggree;

		authService.register(vm.userData)

		.then(function(response){

			vm.loading = false;

			close(vm.userData, 500);
		})

		.catch(function(error) {
			vm.error = error.data.errorMessage;
			vm.loading = false;
		});
	}

	// reset form
	vm.resetForm = function() {
		$scope.form.signup.$submitted = false;
	
		vm.userData = {};
		vm.companyData = {};		
		vm.retypePassword = '';
	}

	/*****************************************
	*	
	*			  Signup Tabs
	*		
	*****************************************/

	// deffault active tab
	vm.activeTab = 'tab1';

	// set active tab
	vm.activateTab = function(tab){
		vm.activeTab = tab;
	}

	// show avtive tab
	vm.isActiveTab = function(tab){
		return vm.activeTab === tab;
	}

	/*****************************************
	*	
	*			  Close Modal
	*		
	*****************************************/

	//Close Modal
	vm.closeModal = function(result) {
	    close(result, 500); // close, but give 200ms for bootstrap to animate
	};

	$log.log("SignupModalCtrl");

}]);

softnetApp.common

.directive('accordion', ["$log", function ($log) {
	return {
		restrict: 'A',
		link: function ($scope, $element) {
			$element.smk_Accordion();
		}
	}
}]);


softnetApp.common

.directive('fancybox', ["$compile", "$timeout", function($compile, $timeout){
    return {
        link: function($scope, $element, $attrs) {

            $element.fancybox({
	            maxWidth: 800,
	            maxHeight: 600,
	            fitToView: false,
	            width: '90%',
	            height: '90%',
	            autoSize: false,
	            closeClick: false,
	            openEffect: 'none',
	            closeEffect: 'none',
	            afterLoad: function(){
	            	
                    $timeout(function(){
                        $compile($(".fancybox-inner"))($scope);
                        $scope.$apply();
                    });
                }
	        });


	        $scope.closeFancyBox = function(){
	        	$.fancybox.close();
	        }
        }
    }
}]);


softnetApp.common
.directive('includeReplace', function () {
    return {
        require: 'ngInclude',
        restrict: 'A', /* optional */
        link: function (scope, $element, attrs) {
            $element.replaceWith($element.children());
        }
    };
});
softnetApp.common

.directive('match', ["$parse", function ($parse) {

	return {
		require: 'ngModel',
		link: function (scope, elem, attrs, ctrl) {
			scope.$watch(function () {
				return $parse(attrs.match)(scope) === ctrl.$modelValue;
			}, function (currentValue) {
				ctrl.$setValidity('mismatch', currentValue);
			});
		}
	};
}]);
softnetApp.common

.directive('scrollOnClick', scrollOnClickDirective);

function scrollOnClickDirective($log) {
	return {
		restrict: 'A',
		link: function (scope, element, attrs) {

			var time = parseInt(attrs.scrollOnClick) || 1000;

			element.on("click", function() {
				var scrollTo = $(scope.scrollTo).offset().top - 120;
				$("html, body").animate({scrollTop: scrollTo}, time);
			});

		},
		scope: {
			scrollTo: '@'
		}
	}
}
softnetApp.common

.directive('sharethis', ["$log", "$timeout", function($log, $timeout) {
    return {
        restrict: 'EA',
        scope: {
            shareUrl: '@',
            buttonType: '@'
        },
        replace: true,
        template: function(tElem, tAttrs){
            if (tAttrs.buttonType === 'large') {
                return "<div><span class='st_sharethis_large' st_title='Softnet' st_url='" + tAttrs.shareUrl + "'' st_summary=''></span></div>"
            } 
            else if (tAttrs.buttonType === 'count'){
               return  "<div><span class='st_fblike_hcount' displayText='Facebook Like'></span>" + 
                        "<span class='st_fbrec_hcount' displayText='Facebook Recommend'></span>" + 
                        "<span class='st_plusone_hcount' displayText='Google +1'></span>" + 
                        "<span class='st_twitterfollow_hcount' displayText='Twitter Follow' st_username='softnet'></span></div>"; 
            }
            else {
                return "<div></div>";
            }
        },
        link: function(scope, element, attr) {

            scope.$watch('shareUrl', function(v) {
                $timeout(function () {
                    if (typeof stButtons.locateElements === 'function') {
                        stButtons.locateElements();
                    }
                });
            });
        }
    };
}]);
softnetApp.common

.directive('tooltip', ["$log", function ($log) {
	return {
		restrict: 'A',
		link: function ($scope, $element) {
			$element.tooltip();
		}
	}
}]);


/*
*	Helper service
*/

softnetApp.common

.factory("helperService", ["$http", "$log", "$q", function($http, $log, $q) {
	return {
		getCountries: function(){
			return $http.get('assets/data/countries.json')

			.then(function(result){
				return result.data;
			});
		},
		whiteList: function(object, whiteList) {
			for (var key in object) {
				// Remove key if not found in whitelist
				if (whiteList.indexOf(key) < 0) {
					delete object[key];
				}
			}

			return this.promiseMaker(object);
		},
		blackList: function(object, blackList) {
			for (var key in object) {
				// Remove the key if is in blacklist
				if (blackList.indexOf(key) >= 0) {
					delete object[key];
				}
			}

			return this.promiseMaker(object);
		},
		promiseMaker: function(data){
			return $q(function(resolve, reject) {
				resolve(data);
			});
		}
	}
}]);
/*
*	Users service
*/

softnetApp.common

.factory("usersService", ["$http", "$log", "Config", "SpringDataRestAdapter", function($http, $log, Config, SpringDataRestAdapter) {
	return {
		getProfile: function(data){
			var httpPromise = $http.get(Config.api.url + '/currentUser', data);

			return SpringDataRestAdapter.process(httpPromise);
		},
		getUserByEmail: function(email){
			var httpPromise = $http.get(Config.api.url + "/user/" + email);

			return SpringDataRestAdapter.process(httpPromise);
		},
		updateUser: function(data){
			var httpPromise = $http.post(Config.api.url + "/updateUser", data);

			return SpringDataRestAdapter.process(httpPromise);
		},
		updateUserPassword: function(data){
			var httpPromise = $http.post(Config.api.url + "/resetPassword", data, {
				headers: { 
					'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
				},
				transformRequest: encodeURIObject
			});

			return SpringDataRestAdapter.process(httpPromise);
		}
	}
}]);
softnetApp.app

.controller("FeedbacksCtrl", ["$log", function ($log) {

	var vm = this; 

	vm.comments = [
		{
			author: 'MR ABC',
			message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ac auctor ipsum. Sed vestibulum auctor risus. Sed a maximus massa. Nullam vehicula quis diam et egestas'
		},
		{
			author: 'MR PQR',
			message: 'Aliquam eu tempus dolor, in sagittis velit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Interdumet malesuada fames ac ante ipsum primis in faucibus. Cras tristique eget ex sed pulvinar.'
		},
		{
			author: 'MR ABC',
			message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ac auctor ipsum. Sed vestibulum auctor risus. Sed a maximus massa. Nullam vehicula quis diam et egestas.'
		},
		{
			author: 'MR PQR',
			message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ac auctor ipsum. Sed vestibulum auctor risus. Sed a maximus massa. Nullam vehicula quis diam et egestas'
		},
		{
			author: 'MR PQR',
			message: 'Aliquam eu tempus dolor, in sagittis velit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Interdumet malesuada fames ac ante ipsum primis in faucibus. Cras tristique eget ex sed pulvinar.'
		}
	];

}]);

softnetApp.app

.controller("ServicesCtrl", ["$log", function ($log) {

	var vm = this; 

	vm.services = [
		{
			title: "CONSULTING",
			icon: "fa-hand-o-right",
			description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
			image: "assets/referrals.jpg",
			link: {
				name: 'CONSULT',
				url: '/'
			}
		},
		{
			title: "MANAGEMENT",
			icon: "fa-search",
			description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
			image: "assets/2.jpg",
			link: {
				name: 'MANAGE',
				url: '/'
			}
		},
		{
			title: "DELIVERY",
			icon: "fa-pencil-square-o",
			description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
			image: "assets/application.jpg",
			link: {
				name: 'DELIVER',
				url: '/'
			}
		}
	];
}]);

softnetApp.app

.controller("SliderCtrl", ["$log", function ($log) {

	var vm = this; 

	vm.slides = [
		{
			img: 'banner1.jpg',
		},
		{
			img: 'banner2.jpg',
		},
		{
			img: 'banner3.jpg',
		}
	];

}]);
